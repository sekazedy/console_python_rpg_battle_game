from classes.game import Person, bcolors
from classes.magic import Spell
from classes.inventory import Item
import random


# Create black magic
fire = Spell("Fire", 25, 600, "black")
thunder = Spell("Thunder", 25, 600, "black")
blizzard = Spell("Blizzard", 25, 600, "black")
meteor = Spell("Meteor", 40, 1200, "black")
quake = Spell("Quake", 14, 140, "black")

# Create light magic
cure = Spell("Cure", 25, 620, "white")
cura = Spell("Cura", 32, 1500, "white")
curaga = Spell("Curaga", 50, 6000, "white")


# Create some Items
potion = Item("Potion", "potion", "Heals 50 HP", 50)
hi_potion = Item("Hi-Potion", "potion", "Heals 100 HP", 100)
super_potion = Item("Super-Potion", "potion", "Heals 1000 HP", 1000)
elixir = Item("Elixir", "elixir", "Fully restores HP/MP of one party member", 9999)
hi_elixir = Item("Hi-Elixir", "elixir", "Fully restores HP/MP of whole party", 9999)

grenade = Item("Grenade", "weapon", "Deals 500 damage", 500)


player_spells = [fire, thunder, blizzard, meteor, cure, cura]
enemy_spells = [fire, quake, curaga]

player_items = [{"item": potion, "quantity": 15},
                {"item": hi_potion, "quantity": 5},
                {"item": super_potion, "quantity": 5},
                {"item": elixir, "quantity": 5},
                {"item": hi_elixir, "quantity": 2},
                {"item": grenade, "quantity": 5}]

# Instantiate Players
player1 = Person("King", 3260, 132, 300, 34, player_spells, player_items)
player2 = Person("Rick", 4160, 188, 311, 34, player_spells, player_items)
player3 = Person("Morty", 3080, 174, 288, 34, player_spells, player_items)

enemy1 = Person("ManBearImp", 1250, 130, 560, 325, enemy_spells, [])
enemy2 = Person("ManBearPig", 18200, 701, 525, 25, enemy_spells, [])
enemy3 = Person("ManBearImp", 1250, 130, 560, 325, enemy_spells, [])

players = [player1, player2, player3]
enemies = [enemy1, enemy2, enemy3]

running = True

print(bcolors.FAIL + bcolors.BOLD + "AN ENEMY ATTACKS!" + bcolors.ENDC)

while running:
    print("===========================")
    print("\n")
    print("NAME\t\t\t\t\t  HP\t\t\t\t\t\t\t\t\tMP")

    for player in players:
        player.get_stats()

    for enemy in enemies:
        enemy.get_enemy_stats()

    for player in players:
        player.choose_action()
        choice = input("\tChoose action:")
        index = int(choice) - 1

        if index == 0:
            dmg = player.generate_damage()
            enemy_idx = player.choose_target(enemies)
            enemies[enemy_idx].take_damage(dmg)
            print("You attacked", enemies[enemy_idx].name, "for", dmg, "points of damage.")

            if enemies[enemy_idx].get_hp() == 0:
                print(enemies[enemy_idx].name, "was defeated!")
                del enemies[enemy_idx]

        elif index == 1:
            player.choose_magic()
            magic_choice = int(input("\tChoose spell:")) - 1

            if magic_choice == -1:
                continue

            spell = player.magic[magic_choice]
            magic_dmg = spell.generate_damage()

            current_mp = player.get_mp()

            if spell.cost > current_mp:
                print(bcolors.FAIL + "\nNot enough MP\n" + bcolors.ENDC)
                continue

            player.reduce_mp(spell.cost)

            if spell.type == "white":
                player.heal(magic_dmg)
                print(bcolors.OKBLUE + spell.name + " heals for", str(magic_dmg), "HP." + bcolors.ENDC)
            elif spell.type == "black":
                enemy_idx = player.choose_target(enemies)
                enemies[enemy_idx].take_damage(magic_dmg)
                print(bcolors.OKBLUE + spell.name + " deals", str(magic_dmg), "points of damage to",
                      enemies[enemy_idx].name, "." + bcolors.ENDC)

                if enemies[enemy_idx].get_hp() == 0:
                    print(enemies[enemy_idx].name, "was defeated!")
                    del enemies[enemy_idx]
        elif index == 2:
            player.choose_item()
            item_choice = int(input("\tChoose item: ")) - 1

            if item_choice == -1:
                continue

            item = player.items[item_choice]["item"]
            if player.items[item_choice]["quantity"] == 0:
                print(bcolors.FAIL + "\n" + "None left..." + bcolors.ENDC)
                continue

            player.items[item_choice]["quantity"] -= 1

            if item.type == "potion":
                player.heal(item.prop)
                print(bcolors.OKGREEN + "\n" + item.name + " heals for", str(item.prop), "HP" + bcolors.ENDC)
            elif item.type == "elixir":
                if item.name == "Hi-Elixir":
                    for i in players:
                        i.hp = i.maxhp
                        i.mp = i.maxmp
                else:
                    player.hp = player.maxhp
                    player.mp = player.maxmp
                print(bcolors.OKGREEN + "\n" + item.name + " fully restored HP/MP" + bcolors.ENDC)
            elif item.type == "weapon":
                enemy_idx = player.choose_target(enemies)
                enemies[enemy_idx].take_damage(item.prop)
                print(bcolors.FAIL + "\n" + item.name + " deals", str(item.prop), "points of damage to",
                      enemies[enemy_idx].name + bcolors.ENDC)

                if enemies[enemy_idx].get_hp() == 0:
                    print(enemies[enemy_idx].name, "was defeated!")
                    del enemies[enemy_idx]

    # Check if battle is over
    defeated_enemies = 0
    defeated_players = 0

    for enemy in enemies:
        if enemy.get_hp() == 0:
            defeated_enemies += 1

    for player in players:
        if player.get_hp() == 0:
            defeated_players += 1

    # Check if players won
    if defeated_enemies == 2:
        print(bcolors.OKGREEN + "You win!" + bcolors.ENDC)
        running = False

    # Check if enemies won
    elif defeated_players == 2:
        print(bcolors.FAIL + "Your enemy has defeated you!" + bcolors.ENDC)
        running = False

    print("\n")

    # Enemies attack phase
    for enemy in enemies:
        enemy_choice = random.randrange(0, 2)
        target = random.randrange(0, 3)

        if enemy_choice == 0:
            enemy_dmg = enemy.generate_damage()

            players[target].take_damage(enemy_dmg)
            print(enemy.name, "attacks", players[target].name, "for", enemy_dmg, "points of damage.")

        elif enemy_choice == 1:
            spell, magic_dmg = enemy.choose_enemy_spell()
            enemy.reduce_mp(spell.cost)

            if spell.type == "white":
                enemy.heal(magic_dmg)
                print(bcolors.OKBLUE + spell.name, "heals", enemy.name,
                      "for", str(magic_dmg), "HP." + bcolors.ENDC)
            elif spell.type == "black":
                players[target].take_damage(magic_dmg)
                print(bcolors.OKBLUE + enemy.name + "'s", spell.name, "deals", str(magic_dmg),
                      "points of damage to", players[target].name + bcolors.ENDC)

                if players[target].get_hp() == 0:
                    print(players[target].name, "has fallen!")
                    del players[target]

